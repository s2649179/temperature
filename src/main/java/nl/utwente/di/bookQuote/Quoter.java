package nl.utwente.di.bookQuote;

public class Quoter {

    public double getFahrenheit(int degree) {
        return (degree * 9/5) + 32;
    }
}
