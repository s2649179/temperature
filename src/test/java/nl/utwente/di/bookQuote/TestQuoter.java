package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {

    @Test
    public void testDegree1() throws Exception{
        Quoter quoter = new Quoter();
        double degree = quoter.getFahrenheit(3);
        Assertions.assertEquals(37.4, degree, 0.0, "Degrees in Celsius 1");
    }
}
